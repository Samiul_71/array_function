<?php
//array array_unique ( array $array [, int $sort_flags = SORT_STRING ] )
//Takes an input array and returns a new array without duplicate values.
//Two elements are considered equal if and only if (string) $elem1 === (string) $elem2 i.e.
// when the string representation is the same, the first element will be used.


$input = array("a" => "green", "red", "2" => "green", "blue", "red");
$result = array_unique($input);
print_r($result);
?>