<?php
//array array_values ( array $array )
//array_values() returns all the values from the array and indexes the array numerically
//Returns an indexed array of values.

$array = array("XL", "gold");
print_r(array_values($array));
?>