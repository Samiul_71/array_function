<?php
//number array_sum ( array $array )..array_sum() returns the sum of values in an array.
//Returns the sum of values as an integer or float.

$a = array(10, 4, 6, 8);
echo "sum(a) = " . array_sum($a) . "</br>";

$b = array("a" => 1.2, "b" => 2.3, "c" => 3.4);
echo "sum(b) = " . array_sum($b) . "\n";
?>