<?php
//array array_reverse ( array $array [, bool $preserve_keys = false ] )
//Takes an input array and returns a new array with the order of the elements reversed
//preserve_keys.....If set to TRUE numeric keys are preserved. Non-numeric keys are not affected by this setting and will always be preserved.
//Returns the reversed array.

$input  = array("php", 4.0,"green", "red");
$reversed = array_reverse($input);
//$preserved = array_reverse($input, true);

//print_r($input);
print_r($reversed);
//print_r($preserved);
?>