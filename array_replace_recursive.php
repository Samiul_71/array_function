<?php
//array array_replace_recursive ( array $array1 , array $array2 [, array $... ] )

$base = array('citrus' => array( "orange") , 'berries' => array("blackberry", "raspberry"), );
$replacements = array('citrus' => array('pineapple'), 'berries' => array('blueberry','mango'));

$basket = array_replace_recursive($base, $replacements);
print_r($basket);

//$basket = array_replace($base, $replacements);
//print_r($basket);
?>