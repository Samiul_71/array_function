<?php
//array array_replace ( array $array1 , array $array2 [, array $... ] )(main)
//Returns an array, or NULL if an error occurs.(main)
//array_replace() replaces the values of array1 with values having the same keys in each of the following arrays.
// If a key from the first array exists in the second array, its value will be replaced by the value from the second array.
// If the key exists in the second array, and not the first, it will be created in the first array.
// If a key only exists in the first array, it will be left as is.
// If several arrays are passed for replacement, they will be processed in order, the later arrays overwriting the previous values.
//array_replace() is not recursive : it will replace values in the first array by whatever type is in the second array.


$base = array("orange", "banana", "apple", "raspberry");
$replacements = array(1 => "pineapple", 4 => "cherry");
$replacements2 = array(2 => "grape");
$replacements3 = array(3 => "mango");

$basket = array_replace($base, $replacements,$replacements2,$replacements3);
print_r($basket);
?>