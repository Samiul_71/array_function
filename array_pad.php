<?php
//array array_pad ( array $array , int $size , mixed $value )
//If size is positive then the array is padded on the right
//if it's negative then on the left
//If the absolute value of size is less than or equal to the length of the array then no padding takes place.

$input = array();

$result = array_pad($input,7,  0);
// result is array(12, 10, 9, 0, 0)
print_r($result);

$result = array_pad($input, -7, -1);
// result is array(-1, -1, -1, -1, 12, 10, 9)

$result = array_pad($input, 2, "noop");
// not padded
?>