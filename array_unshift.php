<?php
//int array_unshift ( array &$array , mixed $value1 [, mixed $... ] )
/*array_unshift() prepends passed elements to the front of the array. 
 Note that the list of elements is prepended as a whole, 
 so that the prepended elements stay in the same order.
 All numerical array keys will be modified to start counting from zero while literal keys won't be touched.*/
//Returns the new number of elements in the array.

$queue = array("orange", "banana");
array_unshift($queue, "apple", "raspberry");
print_r($queue);
?>