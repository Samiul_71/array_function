<?php
    //returns the keys, numeric and string, from the array.
	//array array_keys ( array $array [, mixed $search_value [, bool $strict = false ]] )
	
    /*$array = array(0 => 100, "red" => "color");
        print_r(array_keys($array));
    */
    $array = array("blue", "blue", "green", "blue", "blue");
        print_r(array_keys($array, "blue"));

    /*$array = array("color" => array("blue", "red", "green"),"size"  => array("small", "medium", "large"));
        print_r(array_keys($array));*/
?>