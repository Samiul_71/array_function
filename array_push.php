<?php
//int array_push ( array &$array , mixed $value1 [, mixed $... ] )
//array_push() treats array as a stack, and pushes the passed variables onto the end of array.
// The length of array increases by the number of variables pushed
//array....(The input array).
//value1...(The first value to push onto the end of the array).
//Returns the new number of elements in the array.

$stack = array("orange", "banana");
array_push($stack, "apple", "raspberry","mango");
print_r($stack);
?>