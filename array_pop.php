<?php
//mixed array_pop ( array &$array )
//Returns the last value of array. If array is empty (or is not an array), NULL will be returned.

$stack = array("orange", "banana", "apple", "raspberry","Mango");
$fruit = array_pop($stack);
print_r($stack);
?>