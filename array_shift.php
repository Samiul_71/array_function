<?php
//mixed array_shift ( array &$array )
/*array_shift() shifts the first value of the array off and returns it, 
shortening the array by one element and moving everything down.
All numerical array keys will be modified to start counting from zero while literal keys won't be touched.
*/
$stack = array( "banana", "apple", "raspberry");
$fruit = array_shift($stack);
print_r($stack);
?>