<?php
//mixed array_search ( mixed $needle , array $haystack [, bool $strict = false ] )
//needle.....The searched value.
/*strict....If the third parameter strict is set to TRUE then the array_search() function will search for
identical elements in the haystack.
 This means it will also check the types of the needle in the haystack,
 and objects must be the same instance*/
//Returns the key for needle if it is found in the array, FALSE otherwise.

$array = array('blue', 'red', 'green',  'red');

$key = array_search('green', $array); // $key = 2;
var_dump($key);
$key = array_search('red', $array);   // $key = 1;
?>